/***************************************************************************
 PURPOSE
     RXduino framework

 TARGET DEVICE
     RX63N

 AUTHOR
     Renesas Solutions Corp.
     AND Technology Research Ltd.

 ***************************************************************************
 Copyright (C) 2014 Renesas Electronics. All rights reserved.

 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option) any
 later version.

 See file LICENSE.txt for further informations on licensing terms.

 ***************************************************************************
 Email       : ed.king@andtr.com
 Website     : www.andtr.com
 Module      : EEPROM
 File        : EEPROM.h
 Author      : E King
 Start Date  : 23/10/13
 Description : Declaration of the EEPROM_t interface.

 ******************************************************************************/
/*
 *  Modified 25 July 2014 by Yuuki Okamiya
 */

#ifndef EEPROM_HPP_
#define EEPROM_HPP_

#include "Arduino.h"
#include "utilities/r_flash_api_rx_if.h"

// DEFINITIONS ****************************************************************/

/** EEPROM definitions. */
#define EEPROM_NUM_BLOCKS       16U
#define EEPROM_BLOCK_SIZE_BYTES 2048U
#define EEPROM_WRITE_SIZE_BYTES 2U

/** Definitions of EEPROM block addresses (2048 bytes in size). */
#define EEPROM_ADR_BLOCK0   DF_ADDRESS
#define EEPROM_ADR_BLOCK1   DF_ADDRESS + ( 1U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK2   DF_ADDRESS + ( 2U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK3   DF_ADDRESS + ( 3U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK4   DF_ADDRESS + ( 4U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK5   DF_ADDRESS + ( 5U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK6   DF_ADDRESS + ( 6U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK7   DF_ADDRESS + ( 7U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK8   DF_ADDRESS + ( 8U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK9   DF_ADDRESS + ( 9U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK10  DF_ADDRESS + (10U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK11  DF_ADDRESS + (11U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK12  DF_ADDRESS + (12U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK13  DF_ADDRESS + (13U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK14  DF_ADDRESS + (14U * DF_BLOCK_SIZE_LARGE)
#define EEPROM_ADR_BLOCK15  DF_ADDRESS + (15U * DF_BLOCK_SIZE_LARGE)

/** Definitions of EEPROM block numbers. */
#define EEPROM_BLOCK0   BLOCK_DB0
#define EEPROM_BLOCK1   BLOCK_DB1
#define EEPROM_BLOCK2   BLOCK_DB2
#define EEPROM_BLOCK3   BLOCK_DB3
#define EEPROM_BLOCK4   BLOCK_DB4
#define EEPROM_BLOCK5   BLOCK_DB5
#define EEPROM_BLOCK6   BLOCK_DB6
#define EEPROM_BLOCK7   BLOCK_DB7
#define EEPROM_BLOCK8   BLOCK_DB8
#define EEPROM_BLOCK9   BLOCK_DB9
#define EEPROM_BLOCK10  BLOCK_DB10
#define EEPROM_BLOCK11  BLOCK_DB11
#define EEPROM_BLOCK12  BLOCK_DB12
#define EEPROM_BLOCK13  BLOCK_DB13
#define EEPROM_BLOCK14  BLOCK_DB14
#define EEPROM_BLOCK15  BLOCK_DB15

// DECLARATIONS ***************************************************************/

/**
 * Provides non-volatile storage.
 * @author  E King
 * @version 1.00
 * @since   1.02
 */
class EEPROM_t
{
public:
    /**
     * Default constructor.
     */
    EEPROM_t();
    /**
     * Default destructor.
     */
    ~EEPROM_t();
    /**
     * Read a byte from non-volatile storage.
     * @param   address : to read from.
     * @return  The 2 bytes read.
     */
    unsigned short read(int address);
    /**
     * Write data to non-volatile storage. The minimum write size is 2 bytes.
     * Any part of flash may only be written to once before it has to be re-
     * initialised (erased). The minimum erase size is currently 1 block.
     * @param   address : to write to.
     * @param   value : 2 bytes to write.
     */
    void write(int address, unsigned short value);
    /**
     * Erase a block of flash.
     * @param   block : the block to erase (EEPROM_BLOCK0 to EEPROM_BLOCK15).
     * @return  True if successful, false otherwise.
     */
    bool erase(byte block);

private:
    /** This module's name. */
    static const char *MODULE;
};

#ifdef USING_RXDUINO
class EEPROM : public EEPROM_t{
};
#else
/** External reference to the singular EEPROM object. */
extern EEPROM_t EEPROM;
#endif

#endif // EEPROM_HPP_
