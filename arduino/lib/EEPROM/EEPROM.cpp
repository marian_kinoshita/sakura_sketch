/***************************************************************************
 PURPOSE
     RX63N framework

 TARGET DEVICE
     RX63N

 AUTHOR
     Renesas Electronics Corp.
     AND Technology Research Ltd.

***************************************************************************
 Copyright (C) 2014 Renesas Electronics. All rights reserved.

 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option) any
 later version.

 See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Email       : ed.king@andtr.com
 Website     : www.andtr.com
 Module      : EEPROM
 File        : EEPROM.cpp
 Author      : E King
 Start Date  : 23/10/13
 Description : Implementation of the EEPROM_t interface.

******************************************************************************/

#include "log.h"
#include "EEPROM.h"

// DEFINITIONS ****************************************************************/

// DECLARATIONS ***************************************************************/

/** The global EEPROM object. */
EEPROM_t EEPROM;

/** This module's name. */
const char *EEPROM_t::MODULE = "EEPROM";

// IMPLEMENTATIONS ************************************************************/

/**
 * Default constructor.
 */
EEPROM_t::EEPROM_t()
{
    // Allow read/write access to the whole of data flash.
    R_FlashDataAreaAccess(0xFFFF, 0xFFFF);
}

/**
 * Default destructor.
 */
EEPROM_t::~EEPROM_t()
{

}

/**
 * Read data from non-volatile storage.
 */
unsigned short EEPROM_t::read(int address)
{
    // Just ensure the address is >0, then pass the buck to the underlying flash
    // API, which handles error cases.
    if (address < 0)
    {
        address = 0;
    }
    unsigned short data = 0U;
    memcpy(&data, (const void*)(address), 2U);
    return data;
}

/**
 * Write data to non-volatile storage. The minimum write size is 2 bytes.
 * Any part of flash may only be written to once before it has to be re-
 * initialised (erased). The minimum erase size is currently 1 block.
 */
void EEPROM_t::write(int address, unsigned short value)
{
    // Just ensure the address is >0, then pass the buck to the underlying flash
    // API, which handles error cases.
    if (address < 0)
    {
        address = 0;
    }
    if (FLASH_SUCCESS != R_FlashWrite(address, (uint32_t)&value, 2U))
    {
        LOG_ERROR(MODULE, "write : failed.");
    }
}

/**
 * Erase a block of flash.
 */
bool EEPROM_t::erase(byte block)
{
    if (block >= EEPROM_BLOCK0 && block <= EEPROM_BLOCK15)
    {
        if (FLASH_SUCCESS != R_FlashErase(block))
        {
            LOG_ERROR(MODULE, "erase : failed.");
            return false;
        }
        return true;
    }
    return false;
}
