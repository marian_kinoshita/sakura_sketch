/***************************************************************************
 PURPOSE
     RX63N Library for Arduino compatible framework

 TARGET DEVICE
     RX63N

 AUTHOR
     Renesas Electronics Corp.

 ***************************************************************************
 Copyright (C) 2014 Renesas Electronics. All rights reserved.

 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option) any
 later version.

 See file LICENSE.txt for further informations on licensing terms.

****************************************************************************/
#ifndef _SDMMC_H_
#define _SDMMC_H_

#include <SD.h>

class SDMMC : public SD_t
{
};


#endif/*_SDMMC_H_*/
