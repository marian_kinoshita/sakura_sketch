/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Copyright   : (c) AND Technology Research Ltd, 2013
 Address     : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
 Tel         : +44 (0) 1992 81 4655
 Fax         : +44 (0) 1992 81 3362
 Email       : billy.wood@andtr.com
 Website     : www.andtr.com

 Project     : Arduino
 Module      : File
 File        : File.h
 Author      : B Wood
 Start Date  : 13/12/13
 Description : Declaration of the File interface. This is based on the standard
               Arduino implementation.

 ******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings
 */

#ifndef FILE_HPP_
#define FILE_HPP_

#include "core/Arduino.h"
#include "core/Stream.h"
#include "core/Print.h"
#include "sdfatlib/SdFat.h"
#include "sdfatlib/SdFatUtil.h"
#include "sdfatlib/SdFile.h"

// DEFINITIONS ****************************************************************/


// DECLARATIONS ***************************************************************/

/**
 * Provides FAT file facilities.
 * @author  B Wood
 * @version 1.00
 * @since   1.03
 */
class File : public Stream
{
public:
    /**
     * Default constructor.
     */
    File();
    /**
     * Default destructor.
     */
    ~File();
    /**
     * Constructor with initial setup, to wrap SdFile.
     * @param   f : the object to wrap.
     * @param   name : the name of the file.
     */
    File(SdFile f, const char *name);
    /**
     * @see Print::write
     */
    virtual size_t write(byte);
    /**
     * @see Print::write
     */
    virtual size_t write(const uint8_t *buf, size_t size);
    /**
     * @see Stream::read
     */
    virtual int read();
    /**
     * @see Stream::peek
     */
    virtual int peek();
    /**
     * @see Stream::available
     */
    virtual int available();
    /**
     * @see Stream::flush
     */
    virtual void flush();
    /**
     * Read a file.
     * @param   buf : to read into.
     * @param   nbyte : number of bytes to read.
     * @return  Bytes read.
     */
    int read(void *buf, uint16_t nbyte);
    /**
     * Seek to a position.
     * @param   pos : to seek to.
     * @return  True if successful.
     */
    boolean seek(uint32_t pos);
    /**
     * Current file pointer position.
     * @return  Position.
     */
    uint32_t position();
    /**
     * Get file size.
     * @return  The size.
     */
    uint32_t size();
    /**
     * Close the file.
     */
    void close();
    /**
     * Overload the boolean operator.
     */
    operator bool();
    /**
     * Getter for the filename.
     * @return  The name.
     */
    char *name();
    /**
     * Whether or not we are a directory.
     * @return  True if directory.
     */
    boolean isDirectory();
    /**
     * Open the next file.
     * @param   mode : to open the next file in.
     * @return  The next file.
     */
    File openNextFile(uint8_t mode = O_RDONLY);
    /**
     * Rewind to the first file in the directory.
     */
    void rewindDirectory();

    // Pull in Print::write.
    using Print::write;

private:
    /** This module's name. */
    static const char *MODULE;

    /** Our name. */
    char m_name[13];
    /** Underlying file pointer. */
    SdFile *m_file;
};

#endif // FILE_HPP_
