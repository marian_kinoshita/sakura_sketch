/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Copyright   : (c) AND Technology Research Ltd, 2013
 Address     : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
 Tel         : +44 (0) 1992 81 4655
 Fax         : +44 (0) 1992 81 3362
 Email       : billy.wood@andtr.com
 Website     : www.andtr.com

 Project     : Arduino
 Module      : File
 File        : File.cpp
 Author      : B Wood
 Start Date  : 13/12/13
 Description : Implementation of the File interface.

 ******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings
 */

#include "log.h"
#include "File.h"

// DEFINITIONS ****************************************************************/

// DECLARATIONS ***************************************************************/

/** This module's name. */
const char *File::MODULE = "FILE";

// IMPLEMENTATIONS ************************************************************/

/**
 * Default constructor.
 */
File::File() :
        m_file(NULL)
{
    m_file = 0;
    m_name[0] = 0;
}

/**
 * Default destructor.
 */
File::~File()
{
}

/**
 * Constructor with initial setup, to wrap SdFile.
 */
File::File(SdFile f, const char *name) :
        m_file(NULL)
{
    m_file = (SdFile *) malloc(sizeof(SdFile));
    if (m_file)
    {
        memcpy(m_file, &f, sizeof(SdFile));

        strncpy(m_name, name, 12);
        m_name[12] = 0;
    }
}

/**
 * @see Print::write
 */
size_t File::write(byte b)
{
    return write(&b, 1);
}

/**
 * @see Print::write
 */
size_t File::write(const uint8_t *buf, size_t size)
{
    size_t t;
    if (!m_file)
    {
        setWriteError();
        return 0;
    }
    m_file->clearWriteError();
    t = m_file->write(buf, size);
    if (m_file->getWriteError())
    {
        setWriteError();
        return 0;
    }
    return t;
}

/**
 * @see Stream::read
 */
int File::read()
{
    if (m_file)
        return m_file->read();
    return -1;
}

/**
 * @see Stream::peek
 */
int File::peek()
{
    if (!m_file)
        return 0;

    int c = m_file->read();
    if (c != -1)
        m_file->seekCur(-1);
    return c;
}

/**
 * @see Stream::available
 */
int File::available()
{
    if (!m_file)
        return 0;

    uint32_t n = size() - position();

    return n > 0X7FFF ? 0X7FFF : n;
}

/**
 * @see Stream::flush
 */
void File::flush()
{
    if (m_file)
        m_file->sync();
}

/**
 * Read a file.
 */
int File::read(void *buf, uint16_t nbyte)
{
    if (m_file)
    {
        return m_file->read(buf, nbyte);
    }
    return 0;
}

/**
 * Seek to a position.
 */
boolean File::seek(uint32_t pos)
{
    if (!m_file)
        return false;

    return m_file->seekSet(pos);
}

/**
 * Current file pointer position.
 */
uint32_t File::position()
{
    if (!m_file)
        return -1;
    return m_file->curPosition();
}

/**
 * Get file size.
 */
uint32_t File::size()
{
    if (!m_file)
        return 0;
    return m_file->fileSize();
}

/**
 * Close the file.
 */
void File::close()
{
    if (m_file)
    {
        m_file->close();
        free(m_file);
        m_file = 0;
    }
}

/**
 * Overload the boolean operator.
 */
File::operator bool()
{
    if (m_file)
        return m_file->isOpen();
    return false;
}

/**
 * Getter for the filename.
 */
char *File::name()
{
    return m_name;
}

/**
 * Whether or not we are a directory.
 */
boolean File::isDirectory()
{
    return (m_file && m_file->isDir());
}

/**
 * Open the next file.
 */
File File::openNextFile(uint8_t mode)
{
    dir_t p;

    //Serial.print("\t\treading dir...");
    while (m_file->readDir(&p) > 0)
    {

        // done if past last used entry
        if (p.name[0] == DIR_NAME_FREE)
        {
            //Serial.println("end");
            return File();
        }

        // skip deleted entry and entries for . and  ..
        if (p.name[0] == DIR_NAME_DELETED || p.name[0] == '.')
        {
            //Serial.println("dots");
            continue;
        }

        // only list subdirectories and files
        if (!DIR_IS_FILE_OR_SUBDIR(&p))
        {
            //Serial.println("notafile");
            continue;
        }

        // print file name with possible blank fill
        SdFile f;
        char name[13];
        m_file->dirName(p, name);
        //Serial.print("try to open file ");
        //Serial.println(name);

        if (f.open(m_file, name, mode))
        {
            //Serial.println("OK!");
            return File(f, name);
        }
        else
        {
            //Serial.println("ugh");
            return File();
        }
    }

    //Serial.println("nothing");
    return File();
}

/**
 * Rewind to the first file in the directory.
 */
void File::rewindDirectory()
{
    if (isDirectory())
        m_file->rewind();
}

