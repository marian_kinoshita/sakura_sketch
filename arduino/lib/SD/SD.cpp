/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Copyright   : (c) AND Technology Research Ltd, 2013
 Address     : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
 Tel         : +44 (0) 1992 81 4655
 Fax         : +44 (0) 1992 81 3362
 Email       : billy.wood@andtr.com
 Website     : www.andtr.com

 Project     : Arduino
 Module      : SD
 File        : SD.cpp
 Author      : B Wood
 Start Date  : 13/12/13
 Description : Implementation of the SD_t interface.

 ******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings
 */

#include "log.h"
#include "File.h"
#include "SD.h"
#include "sdfatlib/SdFat.h"

// DEFINITIONS ****************************************************************/

/** Used by get_next_path_component(). */
#define MAX_COMPONENT_LEN 12
#define PATH_COMPONENT_BUFFER_LEN MAX_COMPONENT_LEN+1

// DECLARATIONS ***************************************************************/

/** This module's name. */
const char *SD_t::MODULE = "SD";

// IMPLEMENTATIONS ************************************************************/

/**
 * Get the next path component.
 * E.g. after repeated calls '/foo/bar/baz' will be split
 * into 'foo', 'bar', 'baz'.
 * @param   path : to query.
 * @param   p_offset : to query.
 * @param   buffer : output.
 * @return  True if more components remain.
 */bool getNextPathComponent(char *path, unsigned int *p_offset, char *buffer)
{
    int bufferOffset = 0;

    int offset = *p_offset;

    // Skip root or other separator
    if (path[offset] == '/')
    {
        offset++;
    }

    // Copy the next next path segment
    while (bufferOffset < MAX_COMPONENT_LEN && (path[offset] != '/')
            && (path[offset] != '\0'))
    {
        buffer[bufferOffset++] = path[offset++];
    }

    buffer[bufferOffset] = '\0';

    // Skip trailing separator so we can determine if this
    // is the last component in the path or not.
    if (path[offset] == '/')
    {
        offset++;
    }

    *p_offset = offset;

    return (path[offset] != '\0');

}

/**
 * When given a path, this function traverses the directories in the path and
 * at each level calls the supplied callback function whilst also providing the
 * supplied object for context if required.
 * @param   filepath : to traverse.
 * @param   parentDir : of filepath.
 * @param   callback : to action.
 * @param   object : at level.
 * @return  True if a full directory path is specified and valid.
 */
boolean walkPath(char *filepath, SdFile* parentDir,
        boolean (*callback)(SdFile* parentDir, char *filePathComponent,
                boolean isLastComponent, void *object), void *object = NULL)
{
    SdFile subfile1;
    SdFile subfile2;

    char buffer[PATH_COMPONENT_BUFFER_LEN];

    unsigned int offset = 0;

    SdFile *p_parent;
    SdFile *p_child;

    SdFile *p_tmp_sdfile;

    p_child = &subfile1;

    p_parent = parentDir;

    while (true)
    {

        boolean moreComponents = getNextPathComponent(filepath, &offset,
                buffer);

        boolean shouldContinue = callback(p_parent, buffer, !moreComponents,
                object);

        if (!shouldContinue)
        {
            // TODO: Don't repeat this code?
            // If it's one we've created then we
            // don't need the parent handle anymore.
            if (p_parent != parentDir)
            {
                (*p_parent).close();
            }
            return false;
        }

        if (!moreComponents)
        {
            break;
        }

        boolean exists = (*p_child).open(p_parent, buffer, O_RDONLY);

        // If it's one we've created then we
        // don't need the parent handle anymore.
        if (p_parent != parentDir)
        {
            (*p_parent).close();
        }

        // Handle case when it doesn't exist and we can't continue...
        if (exists)
        {
            // We alternate between two file handles as we go down
            // the path.
            if (p_parent == parentDir)
            {
                p_parent = &subfile2;
            }

            p_tmp_sdfile = p_parent;
            p_parent = p_child;
            p_child = p_tmp_sdfile;
        }
        else
        {
            return false;
        }
    }

    if (p_parent != parentDir)
    {
        (*p_parent).close(); // TODO: Return/ handle different?
    }

    return true;
}

// CALLBACKS ******************************************************************/

/**
 * Callback when path exists.
 * @param   parentDir : parent.
 * @param   filePathComponent : component.
 * @param   isLastComponent : Whether last component.
 * @param   object : at level.
 * @return  True if path exists.
 */
boolean callback_pathExists(SdFile* parentDir, char *filePathComponent,
        boolean isLastComponent, void *object)
{
    SdFile child;

    boolean exists = child.open(parentDir, filePathComponent, O_RDONLY);

    if (exists)
    {
        child.close();
    }

    return exists;
}

/**
 * Callback a when directory is made.
 * @param   parentDir : parent.
 * @param   filePathComponent : component.
 * @param   isLastComponent : Whether last component.
 * @param   object : at level.
 * @return  True if successful.
 */
boolean callback_makeDirPath(SdFile* parentDir, char *filePathComponent,
        boolean isLastComponent, void *object)
{
    boolean result = false;
    SdFile child;

    result = callback_pathExists(parentDir, filePathComponent, isLastComponent,
            object);
    if (!result)
    {
        result = child.makeDir(parentDir, filePathComponent);
    }

    return result;
}

/**
 * Callback when a file is removed.
 * @param   parentDir : parent.
 * @param   filePathComponent : component.
 * @param   isLastComponent : Whether last component.
 * @param   object : at level.
 * @return  True if successful.
 */
boolean callback_remove(SdFile* parentDir, char *filePathComponent,
        boolean isLastComponent, void *object)
{
    if (isLastComponent)
    {
        return SdFile::remove(parentDir, filePathComponent);
    }
    return true;
}

/**
 * Callback when a directory is removed.
 * @param   parentDir : parent.
 * @param   filePathComponent : component.
 * @param   isLastComponent : Whether last component.
 * @param   object : at level.
 * @return  True if successful.
 */
boolean callback_rmdir(SdFile* parentDir, char *filePathComponent,
        boolean isLastComponent, void *object)
{
    if (isLastComponent)
    {
        SdFile f;
        if (!f.open(parentDir, filePathComponent, O_READ))
            return false;
        return f.rmDir();
    }
    return true;
}

// SD IMPLEMENTATION **********************************************************/

/**
 * Default constructor.
 */
SD_t::SD_t() :
        m_file_open_mode(0)
{
}

/**
 * Default destructor.
 */
SD_t::~SD_t()
{
}

/**
 * This needs to be called to set up the connection to the SD card before
 * other methods are used.
 */
boolean SD_t::begin(uint8_t cs_pin)
{
    pinMode(cs_pin, OUTPUT);

    return m_card.init(SPI_SPEED, cs_pin) && m_volume.init(&m_card)
            && m_root.openRoot(&m_volume);
}

/**
 * Open the specified file/directory with the supplied mode (e.g. read or
 * write, etc). Returns a File object for interacting with the file. Note
 * that currently only one file can be open at a time.
 */
File SD_t::open(const char *filename, uint8_t mode)
{
    int pathidx;

    // do the interative search
    SdFile parentdir = get_parent_dir(filename, &pathidx);
    // no more subdirs!

    filename += pathidx;

    if (!filename[0])
    {
        // it was the directory itself!
        return File(parentdir, "/");
    }

    // Open the file itself
    SdFile file;

    // failed to open a subdir!
    if (!parentdir.isOpen())
        return File();

    // there is a special case for the Root directory since its a static dir
    if (parentdir.isRoot())
    {
        if (!file.open(&(SD.m_root), filename, mode))
        {
            // failed to open the file :(
            return File();
        }
        // dont close the root!
    }
    else
    {
        if (!file.open(&parentdir, filename, mode))
        {
            return File();
        }
        // close the parent
        parentdir.close();
    }

    if (mode & (O_APPEND | O_WRITE))
        file.seekSet(file.fileSize());
    return File(file, filename);
}

/**
 * Methods to determine if the requested file path exists.
 */
boolean SD_t::exists(char *filepath)
{
    return walkPath(filepath, &m_root, callback_pathExists);
}

/**
 * Create the requested directory hierarchy - if intermediate directories do
 * not exist they will be created.
 */
boolean SD_t::mkdir(char *filepath)
{
    return walkPath(filepath, &m_root, callback_makeDirPath);
}

/**
 * Delete a file.
 */
boolean SD_t::remove(char *filepath)
{
    return walkPath(filepath, &m_root, callback_remove);
}

/**
 * Delete a directory.
 */
boolean SD_t::rmdir(char *filepath)
{
    return walkPath(filepath, &m_root, callback_rmdir);
}

/**
 * Get the parent directory.
 */
SdFile SD_t::get_parent_dir(const char *filepath, int *index)
{
    // get parent directory
    SdFile d1 = m_root; // start with the mostparent, root!
    SdFile d2;

    // we'll use the pointers to swap between the two objects
    SdFile *parent = &d1;
    SdFile *subdir = &d2;

    const char *origpath = filepath;

    while (strchr(filepath, '/'))
    {

        // get rid of leading /'s
        if (filepath[0] == '/')
        {
            filepath++;
            continue;
        }

        if (!strchr(filepath, '/'))
        {
            // it was in the root directory, so leave now
            break;
        }

        // extract just the name of the next subdirectory
        uint8_t idx = strchr(filepath, '/') - filepath;
        if (idx > 12)
            idx = 12;    // dont let them specify long names
        char subdirname[13];
        strncpy(subdirname, filepath, idx);
        subdirname[idx] = 0;

        // close the subdir (we reuse them) if open
        subdir->close();
        if (!subdir->open(parent, subdirname, O_READ))
        {
            // failed to open one of the subdirectories
            return SdFile();
        }
        // move forward to the next subdirectory
        filepath += idx;

        // we reuse the objects, close it.
        parent->close();

        // swap the pointers
        SdFile *t = parent;
        parent = subdir;
        subdir = t;
    }

    *index = (int) (filepath - origpath);
    // parent is now the parent diretory of the file!
    return *parent;
}

SD_t SD;
