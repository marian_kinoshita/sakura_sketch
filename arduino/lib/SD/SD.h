/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Copyright   : (c) AND Technology Research Ltd, 2013
 Address     : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
 Tel         : +44 (0) 1992 81 4655
 Fax         : +44 (0) 1992 81 3362
 Email       : billy.wood@andtr.com
 Website     : www.andtr.com

 Project     : Arduino
 Module      : SD
 File        : SD.h
 Author      : B Wood
 Start Date  : 13/12/13
 Description : Declaration of the SD interface. This is based-off the standard
               implementation provided by the Arduino standard.

 ******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings
 */

#ifndef SD_HPP_
#define SD_HPP_

#include "core/Arduino.h"
#include "core/Print.h"
#include "File.h"
#include "sdfatlib/SdFat.h"
#include "sdfatlib/SdFatUtil.h"
#include "sdfatlib/SdFile.h"

// DEFINITIONS ****************************************************************/

#define FILE_READ   O_READ
#define FILE_WRITE  (O_READ | O_WRITE | O_CREAT)

#define SPI_SPEED SPI_FULL_SPEED

// DECLARATIONS ***************************************************************/

/**
 * Provides SD card facilities.
 * @author  B Wood
 * @version 1.00
 * @since   1.03
 */
class SD_t
{
public:
    /**
     * Default constructor.
     */
    SD_t();
    /**
     * Default destructor.
     */
    ~SD_t();
    /**
     * This needs to be called to set up the connection to the SD card before
     * other methods are used.
     * @param   cs_pin : the pin connected to the CS line on the SD card.
     * @return  True if successful, false otherwise.
     */
    boolean begin(uint8_t cs_pin = SD_CHIP_SELECT_PIN);
    /**
     * Open the specified file/directory with the supplied mode (e.g. read or
     * write, etc). Returns a File object for interacting with the file. Note
     * that currently only one file can be open at a time.
     * @param   filename : to open (full path).
     * @param   mode : FILE_READ or FILE_WRITE.
     * @return  A File object, which can be evaluated to true/false.
     */
    File open(const char *filename, uint8_t mode = FILE_READ);
    /**
     * Methods to determine if the requested file path exists.
     * @param   filepath : to check.
     * @return  True if exists, false otherwise.
     */
    boolean exists(char *filepath);
    /**
     * Create the requested directory hierarchy - if intermediate directories do
     * not exist they will be created.
     * @param   filepath : to make.
     * @return  True if successful, false otherwise.
     */
    boolean mkdir(char *filepath);
    /**
     * Delete a file.
     * @param   filepath : to delete.
     * @return  True if successful, false otherwise.
     */
    boolean remove(char *filepath);
    /**
     * Delete a directory.
     * @param   filepath : to delete.
     * @return  True if successful, false otherwise.
     */
    boolean rmdir(char *filepath);

private:
    /** This module's name. */
    static const char *MODULE;

    /** Card.*/
    Sd2Card m_card;
    /** Volume. */
    SdVolume m_volume;
    /** Root directory. */
    SdFile m_root;
    /**
     * This is used to determine the mode used to open a file it's here because
     * it's the easiest place to pass the information through the directory
     * walking function. But it's probably not the best place for it.
     * It shouldn't be set directly--it is set via the parameters to `open`.
     */
    int m_file_open_mode;

    /**
     * Get the parent directory.
     * @param   filepath : to query.
     * @param   index : to find.
     * @return  A returned file object.
     */
    SdFile get_parent_dir(const char *filepath, int *index);

    /** Friend class : File. */
    friend class File;
};


extern SD_t SD;

#endif // SD_HPP_
