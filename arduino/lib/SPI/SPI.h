﻿/*
 * Copyright (c) 2010 by Cristian Maglie <c.maglie@bug.st>
 * SPI Master library for arduino.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 *
 * Modified 28 February 2013 by masahiko.nagata.cj@renesas.com
 * Modified 30 April 2014 by Yuuki Okamiya for GR-SAKURA
 * Modified 31 May 2014 by Nozomu Fujita : 構造体 st_rspi の SPSR.BIT を削除したのに伴い SPSR へのアクセス部分を変更した
 *
 */

#ifndef _SPI_H_INCLUDED
#define _SPI_H_INCLUDED

#include <stdio.h>
#include <Arduino.h>

#define SPI_CLOCK_DIV2   0x00
#define SPI_CLOCK_DIV4   0x01
#define SPI_CLOCK_DIV8   0x03
#define SPI_CLOCK_DIV16  0x07
#define SPI_CLOCK_DIV32  0x0F
#define SPI_CLOCK_DIV64  0x1F
#define SPI_CLOCK_DIV128 0x3F

#define SPI_MODE0 0x0
#define SPI_MODE1 0x1
#define SPI_MODE2 0x2
#define SPI_MODE3 0x3

#define SPI_MODE_MASK 0x0003  // PHA = bit 0, POL = 1

class SPIClass {
public:
  inline static byte transfer(byte _data);

  // SPI Configuration methods

//  inline static void attachInterrupt();
//  inline static void detachInterrupt(); // Default

  static void begin(); // Default
  static void end();

  static void setBitOrder(uint8_t);
  static void setDataMode(uint8_t);
  static void setClockDivider(uint8_t);
private:

};

extern SPIClass SPI;

byte SPIClass::transfer(byte _data) {
    st_rspi_spsr spsr;
    spsr.BYTE = RSPI0.SPSR.BYTE;
    if(spsr.BIT.OVRF == 1)
    {
        spsr.BIT.OVRF = 0;
        spsr.BIT.b7 = 1;
        spsr.BIT.b5 = 1;
        RSPI0.SPSR.BYTE = spsr.BYTE;
    }
    RSPI0.SPDR.LONG = (unsigned long)_data;

    while(ICU.IR[39].BIT.IR == 0);
    ICU.IR[39].BIT.IR = 0;
    return (byte)RSPI0.SPDR.LONG;
}

/*
void SPIClass::attachInterrupt() {
#ifdef WORKAROUND_READ_MODIFY_WRITE
  CBI(SFR_IFxx, SFR_BIT_CSIIFxx);
  CBI(SFR_MKxx, SFR_BIT_CSIMKxx);
#else
  SPI_CSIIFxx = 0;
  SPI_CSIMKxx = 0;
#endif
}

void SPIClass::detachInterrupt() {
#ifdef WORKAROUND_READ_MODIFY_WRITE
  CBI(SFR_IFxx, SFR_BIT_CSIIFxx);
  SBI(SFR_MKxx, SFR_BIT_CSIMKxx);
#else
  SPI_CSIIFxx = 0;
  SPI_CSIMKxx = 1;
#endif
}
*/

#endif
