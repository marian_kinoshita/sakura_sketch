﻿/*
 * Copyright (c) 2010 by Cristian Maglie <c.maglie@bug.st>
 * SPI Master library for arduino.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU General Public License version 2
 * or the GNU Lesser General Public License version 2.1, both as
 * published by the Free Software Foundation.
 *
 * Modified 28 February 2013 by masahiko.nagata.cj@renesas.com
 * Modified 30 April 2014 by Yuuki Okamiya for GR-SAKURA
 */

#include "SPI.h"

SPIClass SPI;

void SPIClass::begin() {
    pinMode(SCK, OUTPUT);
    pinMode(MISO, INPUT_PULLUP);
    pinMode(MOSI, OUTPUT);
    pinMode(SS, OUTPUT);

    digitalWrite(SCK, HIGH);
    digitalWrite(MOSI, HIGH);
    digitalWrite(SS, HIGH);

  /* PWPR.PFSWE write protect off */
    MPC.PWPR.BYTE = 0x00u;
  /* PFS register write protect off */
    MPC.PWPR.BYTE = 0x40u;
  /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;

  /* Set pin mode for MOSI, MISO, RSPCK & SSL */
    PORTC.PMR.BIT.B5 = 1;
    MPC.PC5PFS.BIT.PSEL = 0x0d;
    PORTC.PMR.BIT.B6 = 1;
    MPC.PC6PFS.BIT.PSEL = 0x0d;
    PORTC.PMR.BIT.B7 = 1;
    MPC.PC7PFS.BIT.PSEL = 0x0d;
    PORTC.PMR.BIT.B4 = 1;
    MPC.PC4PFS.BIT.PSEL = 0x0d;
    RSPI0.SPCMD0.BIT.SSLA = 0;

    /* Protection off */
    SYSTEM.PRCR.WORD = 0xA503u;
    /* Wake RSPI unit from standby mode */
    MSTP(RSPI0) = 0u;
    /* Protection on */
    SYSTEM.PRCR.WORD = 0xA500u;

    RSPI0.SPCR.BYTE = 0x08u; //SPI Stop, Set to Master Mode
    /* Set SSL pin to active low */
    RSPI0.SSLP.BIT.SSL0P = 0u;
    /* Set SPPCR register */
    RSPI0.SPPCR.BYTE = 0u;
    /* Set bit rate to 12Mbit/s, by setting SPBR to 0 */
    RSPI0.SPBR = SPI_CLOCK_DIV4;
    /* Set SPDCR register */
    RSPI0.SPDCR.BYTE = 0x20u;
    /* Set RSPI sequence control pointer to SPCMD0 */
    RSPI0.SPSCR.BYTE = 0u;
    /* Set SPCMD0 register (command register 0) */
    RSPI0.SPCMD0.WORD = 0x1700u; //LSB first, 8bit, SPI MODE0, SSL0(External)

    RSPI0.SPCR.BIT.SPRIE = 1; // Enable Receive Interrupt Request
    RSPI0.SPCR.BIT.SPE = 1; //Start SPI

}

void SPIClass::end() {
    RSPI0.SPCR.BIT.SPE = 0; //Stop SPI
}

void SPIClass::setBitOrder(uint8_t bitOrder)
{
    RSPI0.SPCR.BIT.SPE = 0; //Stop SPI

    if(bitOrder == LSBFIRST) {
        RSPI0.SPCMD0.WORD |=  (1 << 12);
    } else {
        RSPI0.SPCMD0.WORD &= ~(1 << 12);
    }

    RSPI0.SPCR.BIT.SPE = 1; //Start SPI
}

void SPIClass::setDataMode(uint8_t mode)
{
    RSPI0.SPCR.BIT.SPE = 0; //Stop SPI

    RSPI0.SPCMD0.WORD = (RSPI0.SPCMD0.WORD & ~SPI_MODE_MASK) | ((uint16_t)mode);

    RSPI0.SPCR.BIT.SPE = 1; //Start SPI
}

void SPIClass::setClockDivider(uint8_t rate)
{
    RSPI0.SPCR.BIT.SPE = 0; //Stop SPI

    RSPI0.SPBR = rate;

    RSPI0.SPCR.BIT.SPE = 1; //Start SPI
}


