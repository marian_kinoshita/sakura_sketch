/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     Renesas Solutions Corp.
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
  Copyright  : (c) AND Technology Research Ltd, 2013
  Address    : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
  Tel        : +44 (0) 1992 81 4655
  Fax        : +44 (0) 1992 81 3362
  Email      : ed.king@andtr.com
  Website    : www.andtr.com

  Project    : Arduino
  Module     : Utilities
  File       : log.h
  Author     : E King
  Start Date : 19/02/13
  Description: Declaration of the Log class, which provides debug logging. This
               class has been ported for use in the Arduino project.

 ******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings
 */

#ifndef LOG_HPP_
#define LOG_HPP_

// DEFINES ********************************************************************/

/** Length of a log line. */
#define LOG_LINE_LENGTH 100U

// MACROS *********************************************************************/

/** Logging macros. These should be called from the application. */
#define LOG_DEBUG   Log::log_debug
#define LOG_INFO    Log::log_info
#define LOG_WARNING Log::log_warning
#define LOG_ERROR   Log::log_error
#define LOG_FATAL   Log::log_fatal

// DECLARATIONS ***************************************************************/

/**
 * Enumeration of log levels.
 * @author  E King
 * @version 1.00
 * @since   1.00
 */
enum LogLevel
{
    /** Debug level logging. Severity lowest. */
    ll_debug,
    /** Info level logging. */
    ll_info,
    /** Warning level logging. */
    ll_warning,
    /** Error level logging. */
    ll_error,
    /** Fatal level logging. Severity highest. */
    ll_fatal
};

/**
 * Provides application logging. The methods presented below are wrapped-up
 * in macros that should be called from the application. This class can only
 * has any effect when the DEBUG symbol is #defined.
 * @author  E King
 * @version 1.00
 * @since   1.00
 */
class Log
{
public:
    /**
     * Log a DEBUG level message.
     * @param   module : the originating module.
     * @param   message : the message to log.
     */
    static void log_debug(const char *module, const char *message);
    /**
     * Log an INFO level message.
     * @param   module : the originating module.
     * @param   message : the message to log.
     */
    static void log_info(const char *module, const char *message);
    /**
     * Log a WARNING level message.
     * @param   module : the originating module.
     * @param   message : the message to log.
     */
    static void log_warning(const char *module, const char *message);
    /**
     * Log an ERROR level message.
     * @param   module : the originating module.
     * @param   message : the message to log.
     */
    static void log_error(const char *module, const char *message);
    /**
     * Log a FATAL level message.
     * @param   module : the originating module.
     * @param   message : the message to log.
     */
    static void log_fatal(const char *module, const char *message);
};

#endif // LOG_HPP_
