/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     Renesas Solutions Corp.
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Copyright   : (c) AND Technology Research Ltd, 2013
 Address     : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
 Tel         : +44 (0) 1992 81 4655
 Fax         : +44 (0) 1992 81 3362
 Email       : ed.king@andtr.com
 Website     : www.andtr.com

 Project     : Arduino
 Module      : Core
 File        : Utilities.cpp
 Author      : E King
 Start Date  : 03/09/13
 Description : Implementation of the common utility functions.

 ******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings, fixed timer frequency.
 *  Modified 12 May 2014 by Yuuki Okamiya, modify for analogWriteFrequency
 *  Modified 15 May 2014 by Yuuki Okamiya, change timer channel for each PWM pin.
 *  Modified 18 Jun 2014 by Nozomu Fujita : コメント修正
 *  Modified 19 Jun 2014 by Nozomu Fujita : 周期起動ハンドラ不具合修正
 *  Modified 19 Jun 2014 by Nozomu Fujita : INT_Excep_TPU2_TGI2A() 不具合修正
 *  Modified 19 Jun 2014 by Nozomu Fujita : timer_regist_userfunc() を追加
 */
#include <stddef.h>

#include "Arduino.h"
#include "Time.h"
#include "Utilities.h"
#include "log.h"

#include "rx63n/interrupt_handlers.h"
#include "rx63n/iodefine.h"
#include "rx63n/util.h"

// DEFINITIONS ****************************************************************/
/** The counter period required to give ~490Hz PWM frequency for 12MHz PCLK. */
#define COUNTER_PERIOD_FOR_490HZ_PWM    24490U
/** The maximum permitted frequency of a PWM signal, assuming a 12MHz PCLK. */
#define MAX_PWM_FREQ                    65535U
/** The counter value per duty cycle step for PWM of 490Hz from 12MHz PCLK. */
#define COUNTER_PER_DUTY_CYCLE_STEP     96U
/** The maximum frequency of a analogWrite signal, assuming a 12MHz PCLK. */
#define MAX_ANALOGWRITE_FREQ                    46875U
/** The minimum frequency of a analogWrite signal, assuming a 12MHz PCLK. */
#define MIN_ANALOGWRITE_FREQ                    184U

#define MAX_CYCLIC_HANDLER      (8)         //!< Number of maximum cyclic handler

// MACROS *********************************************************************/

#define COUNTER_INTERVAL

// DECLARATIONS ***************************************************************/
/** The current conversion reference. */
static word g_frequency_analogwrite = 490;
static fITInterruptFunc_t   g_fITInterruptFunc = NULL;  //!< ユーザー定義インターバルタイマハンドラ
// 周期起動ハンドラ関数テーブル
static fITInterruptFunc_t   g_afCyclicHandler[MAX_CYCLIC_HANDLER] =
{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
static uint32_t g_au32CyclicTime[MAX_CYCLIC_HANDLER] =
{ 0, 0, 0, 0, 0, 0, 0, 0};
static uint32_t g_au32CyclicHandlerLastTime[MAX_CYCLIC_HANDLER] =
{ 0, 0, 0, 0, 0, 0, 0, 0};
byte g_pinsCurrentAttachFunction[NUM_DIGITAL_PINS] = {_ATTACH_OTHER};

static word g_count_swpwmPin9 = 0;
static word g_count_swpwmPin10 = 0;
static word g_swPwmPin9_duty_cycle = 0;
static word g_swPwmPin9_counter_period = 0;
static word g_swPwmPin10_duty_cycle = 0;
static word g_swPwmPin10_counter_period = 0;


/** This module's name. */
static const char *MODULE = "UTILITIES";

// IMPLEMENTATIONS ************************************************************/

/**
 * Grant/deny write access to the PFS registers.
 */
void grant_pfs_write_access(bool grant)
{
    // Clear the PFSWE enable bit.
    MPC.PWPR.BIT.B0WI = 0U;

    if (grant)
    {
        // Set the PFS enable bit.
        MPC.PWPR.BIT.PFSWE = 1U;
    }
    else
    {
        // Clear the PFS enable bit.
        MPC.PWPR.BIT.PFSWE = 0U;
    }

    // Reset the PFSWE enable bit.
    MPC.PWPR.BIT.B0WI = 1U;
}

bool set_frequency_for_analogwrite(word frequency)
{
    if (frequency < MIN_ANALOGWRITE_FREQ || frequency > MAX_ANALOGWRITE_FREQ){
        return false;
    }
    g_frequency_analogwrite = frequency;
    return true;
}

/**
 * Helper function to write a PWM signal to a pin of a given frequency and
 * duty cycle.
 */
bool set_pwm_for_pin(int pin, uint16_t frequency, byte value,
        bool for_analog_write)
{
    bool success = false;
    word counter_period, duty_cycle;

    if (pin != INVALID_IO)
    {
        byte clock_divisor = 0b001; //12MHz
        if (for_analog_write)
        {
            // Calculate the duty cycle we're going to use.
            // The default setting of frequency is 490Hz, but can be
            // set by analogWriteFrequency in AnalogIO.cpp

            counter_period = 12000000 / g_frequency_analogwrite;
            if (value >= 0)
            {
                duty_cycle = counter_period - (counter_period * value / 255);
            }
            if (value >= 255)
            {
                duty_cycle = 0U;
            }
        } // if : for analog write.
        else
        {
            // Check the frequency is in range.
            if (frequency > MAX_PWM_FREQ)
            {
                frequency = MAX_PWM_FREQ;
            }
            if (frequency == 0U)
            {
                frequency = 1U;
            }
            // Determine the PCLK divisor required for the given frequency. Note
            // that for low frequencies only certain pins can be used as the
            // larger divisors aren't available in all MTU modules. All of these
            // calculations for minimum frequency assume a 12MHz PCLK.
            // Frequencies of < 12Hz are only possible on MTUs 2, 3 and 4.
            if (frequency < 12U)
            {
                if (    pin == PIN_IO34 || //
                        pin == PIN_IO22 || pin == PIN_IO11 || //
                        pin == PIN_IO55 || pin == PIN_IO4  || //
                        pin == PIN_LED0 || pin == PIN_IO46 || //
                        pin == PIN_IO5  || pin == PIN_IO45 ||
                        pin == PIN_IO3)
                {
                    // PCLK/1024 (for MTU3, MTU4 and TPU3 ).
                    clock_divisor = 0b101;
                    // Ticks per Hz = 46875.
                    counter_period = 46875U / frequency;
                } // if : pin check for low frequency.
                else
                {
                    LOG_INFO(MODULE, "set_pwm_for_pin: " //
                            "Frequency too low for pin.");
                    return success;
                }
            } // if : freq < 12Hz.
            // Frequencies of < 184Hz are possible on all MTUs.
            else if (frequency < 184U)
            {
                // PCLK/64.
                clock_divisor = 0b011;
                // Range of 3 to 183Hz.
                counter_period = 750000U / frequency;
            }
            else
            {
                counter_period = 12000000 / frequency;
            }
            // Duty cycle always 50% for tone.
            duty_cycle = counter_period / 2U;
        } // else : for tone.

        if ((g_pinsCurrentAttachFunction[pin] == _ATTACH_ANALOGWRITE) ||
                g_pinsCurrentAttachFunction[pin] == _ATTACH_TONE){
            if (pin == PIN_IO9 || pin == PIN_IO10){
                return rapid_set_swPwm_for_pin(pin, frequency, value, for_analog_write);
            } else {
                return rapid_set_pwm_for_pin(pin, clock_divisor, counter_period, duty_cycle);
            }
        }

        // Grant access to the PFS registers and the CGC registers.
        grant_pfs_write_access(true);
        startModule(MstpIdMTU);
        startModule(MstpIdTPU0);

        // Determine which pin we're trying to write to. This will determine
        // which multi-function timer unit we need to use.
        // TPU0, TIOCC0 ------------------------------------------------------/
        if (pin == PIN_IO6)
        {
            // Configure the pin to use the peripheral module as an output.
            PORT3.PDR.BIT.B2 = 1U;
            PORT3.PMR.BIT.B2 = 1U;

            // Set the MTU function, and disable other pins on this channel.
            MPC.P32PFS.BIT.PSEL = 0b00011;

            // Stop the PWM signal.
            TPUA.TSTR.BIT.CST0 = 0U;
            // Set the counter to run at the desired frequency (12MHz*divisor).
            TPU0.TCR.BIT.TPSC = clock_divisor;
            // Set TGRA compare match to clear TCNT.
            TPU0.TCR.BIT.CCLR = 0b001;
            // Set the count to occur on rising edge of PCLK.
            TPU0.TCR.BIT.CKEG = 0b01;
            // Set PWM mode 2.
            TPU0.TMDR.BIT.MD = 0b0011;
            // Set TGRA to have an initial value of 0 and clear to 0.
            TPU0.TIORH.BIT.IOA = 0b0001;
            // Set TGRD to have an initial value of 0 and clear to 1.
            TPU0.TIORL.BIT.IOC = 0b0010;
            // Set the period.
            TPU0.TGRA = counter_period;
            // Set the duty cycle in TGRC.
            TPU0.TGRC = duty_cycle;
            // Start the PWM signal.
            TPUA.TSTR.BIT.CST0 = 1U;
            // Update the success flag.
            success = true;
        }
        // TPU0, TIOCD0 ------------------------------------------------------/
        else if (pin == PIN_IO7)
        {
            // Configure the pin to use the peripheral module as an output.
            PORT3.PDR.BIT.B3 = 1U;
            PORT3.PMR.BIT.B3 = 1U;

            // Set the MTU function, and disable other pins on this channel.
            MPC.P33PFS.BIT.PSEL = 0b00011;

            // Stop the PWM signal.
            TPUA.TSTR.BIT.CST0 = 0U;
            // Set the counter to run at the desired frequency (12MHz*divisor).
            TPU0.TCR.BIT.TPSC = clock_divisor;
            // Set TGRA compare match to clear TCNT.
            TPU0.TCR.BIT.CCLR = 0b001;
            // Set the count to occur on rising edge of PCLK.
            TPU0.TCR.BIT.CKEG = 0b01;
            // Set PWM mode 2.
            TPU0.TMDR.BIT.MD = 0b0011;
            // Set TGRA to have an initial value of 0 and clear to 0.
            TPU0.TIORH.BIT.IOA = 0b0001;
            // Set TGRC to have an initial value of 0 and clear to 1.
            TPU0.TIORL.BIT.IOD = 0b0010;
            // Set the period.
            TPU0.TGRA = counter_period;
            // Set the duty cycle in TGRC.
            TPU0.TGRD = duty_cycle;
            // Start the PWM signal.
            TPUA.TSTR.BIT.CST0 = 1U;
            // Update the success flag.
            success = true;
        }
        // MTU1, MTIOC1A ------------------------------------------------------/
        else if (pin == PIN_IO1 || pin == PIN_IO48)
        {
            switch (pin)
            {
            case PIN_IO1:
                PORT2.PDR.BIT.B0 = 1U;
                PORT2.PMR.BIT.B0 = 1U;
                MPC.P20PFS.BIT.ISEL = 0U;
                MPC.P20PFS.BIT.PSEL = 0b00001;
                MPC.PE4PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO48:
                PORTE.PDR.BIT.B4 = 1U;
                PORTE.PMR.BIT.B4 = 1U;
                MPC.P20PFS.BIT.PSEL = 0U;
                MPC.PE4PFS.BIT.PSEL = 0b00010;
                break;
            default:
                break;
            }

            // Each MTU unit has different register settings to achieve the same
            // result - see the hardware manual for details.
            MTU.TSTR.BIT.CST1 = 0U;
            MTU1.TCR.BIT.TPSC = clock_divisor;
            MTU1.TCR.BIT.CCLR = 0b001;
            MTU1.TCR.BIT.CKEG = 0b01;
            MTU1.TMDR.BIT.MD = 0b0010;
            MTU1.TIOR.BIT.IOA = 0b0001;
            MTU1.TIOR.BIT.IOB = 0b0010;
            MTU1.TGRA = counter_period;
            MTU1.TGRB = duty_cycle;
            MTU.TSTR.BIT.CST1 = 1U;
            success = true;
        }

        /* removed because MTIOC3C is used for PIN11, so that influence to clearTCNT
        // MTU3, MTIOC3A ------------------------------------------------------/
        else if (pin == PIN_IO32 || pin == PIN_IO35 || pin == PIN_IO23 || //
                pin == PIN_IO12)
        {
            switch (pin)
            {
            case PIN_IO32:
                PORT1.PDR.BIT.B4 = 1U;
                PORT1.PMR.BIT.B4 = 1U;
                MPC.P14PFS.BIT.ISEL = 0U;
                MPC.P14PFS.BIT.PSEL = 0b00001;
                MPC.P17PFS.BIT.PSEL = 0U;
                MPC.PC1PFS.BIT.PSEL = 0U;
                MPC.PC7PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO35:
                PORT1.PDR.BIT.B7 = 1U;
                PORT1.PMR.BIT.B7 = 1U;
                MPC.P14PFS.BIT.PSEL = 0U;
                MPC.P17PFS.BIT.ISEL = 0U;
                MPC.P17PFS.BIT.PSEL = 0b00001;
                MPC.PC1PFS.BIT.PSEL = 0U;
                MPC.PC7PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO23:
                PORTC.PDR.BIT.B1 = 1U;
                PORTC.PMR.BIT.B1 = 1U;
                MPC.P14PFS.BIT.PSEL = 0U;
                MPC.P17PFS.BIT.PSEL = 0U;
                MPC.PC1PFS.BIT.ISEL = 0U;
                MPC.PC1PFS.BIT.PSEL = 0b00001;
                MPC.PC7PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO12:
                PORTC.PDR.BIT.B7 = 1U;
                PORTC.PMR.BIT.B7 = 1U;
                MPC.P14PFS.BIT.PSEL = 0U;
                MPC.P17PFS.BIT.PSEL = 0U;
                MPC.PC1PFS.BIT.PSEL = 0U;
                MPC.PC7PFS.BIT.ISEL = 0U;
                MPC.PC7PFS.BIT.PSEL = 0b00001;
                break;
            default:
                break;
            }

            MTU.TSTR.BIT.CST3= 0U;
            MTU3.TCR.BIT.TPSC = clock_divisor;
            MTU3.TCR.BIT.CCLR = 0b001;
            MTU3.TCR.BIT.CKEG = 0b01;
            MTU3.TMDR.BIT.MD = 0b0010;
            MTU3.TIORH.BIT.IOA = 0b0001;
            MTU3.TIORH.BIT.IOB = 0b0010;
            MTU3.TGRA = counter_period;
            MTU3.TGRB = duty_cycle;
            MTU.TSTR.BIT.CST3 = 1U;
            success = true;
        }
        */

        // MTU3, MTIOC3C ------------------------------------------------------/
        else if (pin == PIN_IO34 || pin == PIN_IO22 || pin == PIN_IO11 || //
                pin == PIN_IO55)
        {
            switch (pin)
            {
            case PIN_IO34:
                PORT1.PDR.BIT.B6 = 1U;
                PORT1.PMR.BIT.B6 = 1U;
                MPC.P16PFS.BIT.ISEL = 0U;
                MPC.P16PFS.BIT.PSEL = 0b00001;
                MPC.PC0PFS.BIT.PSEL = 0U;
                MPC.PC6PFS.BIT.PSEL = 0U;
                MPC.PJ3PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO22:
                PORTC.PDR.BIT.B0 = 1U;
                PORTC.PMR.BIT.B0 = 1U;
                MPC.P16PFS.BIT.PSEL = 0U;
                MPC.PC0PFS.BIT.ISEL = 0U;
                MPC.PC0PFS.BIT.PSEL = 0b00001;
                MPC.PC6PFS.BIT.PSEL = 0U;
                MPC.PJ3PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO11:
                PORTC.PDR.BIT.B6 = 1U;
                PORTC.PMR.BIT.B6 = 1U;
                MPC.P16PFS.BIT.PSEL = 0U;
                MPC.PC0PFS.BIT.PSEL = 0U;
                MPC.PC6PFS.BIT.ISEL = 0U;
                MPC.PC6PFS.BIT.PSEL = 0b00001;
                MPC.PJ3PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO55:
                PORTJ.PDR.BIT.B3 = 1U;
                PORTJ.PMR.BIT.B3 = 1U;
                MPC.P16PFS.BIT.PSEL = 0U;
                MPC.PC0PFS.BIT.PSEL = 0U;
                MPC.PC6PFS.BIT.PSEL = 0U;
                MPC.PJ3PFS.BIT.PSEL = 0b00001;
                break;
            default:
                break;
            }

            MTU.TSTR.BIT.CST3 = 0U;
            MTU3.TCR.BIT.TPSC = clock_divisor;
            MTU3.TCR.BIT.CCLR = 0b101;
            MTU3.TCR.BIT.CKEG = 0b01;
            MTU3.TMDR.BIT.MD = 0b0010;
            MTU3.TIORL.BIT.IOC = 0b0001;
            MTU3.TIORL.BIT.IOD = 0b0010;
            MTU3.TGRC = counter_period;
            MTU3.TGRD = duty_cycle;
            MTU.TSTR.BIT.CST3 = 1U;
            success = true;
        }
        // TPU4, TIOCA4 ------------------------------------------------------/
        else if (pin == PIN_IO5)
        {
            // Configure the pin to use the peripheral module as an output.
            PORT2.PDR.BIT.B5 = 1U;
            PORT2.PMR.BIT.B5 = 1U;

            // Set the MTU function, and disable other pins on this channel.
            MPC.P25PFS.BIT.PSEL = 0b00011;

            // Stop the PWM signal.
            TPUA.TSTR.BIT.CST4 = 0U;
            // Set the counter to run at the desired frequency (12MHz*divisor).
            TPU4.TCR.BIT.TPSC = clock_divisor;
            // Set TGRA compare match to clear TCNT.
            TPU4.TCR.BIT.CCLR = 0b001;
            // Set the count to occur on rising edge of PCLK.
            TPU4.TCR.BIT.CKEG = 0b01;
            // Set PWM mode 1.
            TPU4.TMDR.BIT.MD = 0b0010;
            // Set TGRA to have an initial value of 0 and clear to 0.
            TPU4.TIOR.BIT.IOA = 0b0001;
            // Set TGRD to have an initial value of 0 and clear to 1.
            TPU4.TIOR.BIT.IOB = 0b0010;
            // Set the period.
            TPU4.TGRA = counter_period;
            // Set the duty cycle in TGRC.
            TPU4.TGRB = duty_cycle;
            // Start the PWM signal.
            TPUA.TSTR.BIT.CST4 = 1U;
            // Update the success flag.
            success = true;
        }
        // MTU4, MTIOC4A ------------------------------------------------------/
        else if (pin == PIN_IO4 || pin == PIN_LED1 || pin == PIN_IO46)
        {
            switch (pin)
            {
            case PIN_IO4:
                PORT2.PDR.BIT.B4 = 1U;
                PORT2.PMR.BIT.B4 = 1U;
                MPC.P24PFS.BIT.PSEL = 0b00001;
                MPC.PA0PFS.BIT.PSEL = 0U;
                MPC.PE2PFS.BIT.PSEL = 0U;
                break;
            case PIN_LED1:
                PORTA.PDR.BIT.B0 = 1U;
                PORTA.PMR.BIT.B0 = 1U;
                MPC.P24PFS.BIT.PSEL = 0U;
                MPC.PA0PFS.BIT.PSEL = 0b00001;
                MPC.PE2PFS.BIT.PSEL = 0U;
                break;
            case PIN_IO46:
                PORTE.PDR.BIT.B2 = 1U;
                PORTE.PMR.BIT.B2 = 1U;
                MPC.P24PFS.BIT.PSEL = 0U;
                MPC.PA0PFS.BIT.PSEL = 0U;
                MPC.PE2PFS.BIT.ISEL = 0U;
                MPC.PE2PFS.BIT.PSEL = 0b00001;
                break;
            default:
                break;
            }

            MTU.TSTR.BIT.CST4 = 0U;
            MTU4.TCR.BIT.TPSC = clock_divisor;
            MTU4.TCR.BIT.CCLR = 0b001;
            MTU4.TCR.BIT.CKEG = 0b01;
            MTU4.TMDR.BIT.MD = 0b0010;
            MTU4.TIORH.BIT.IOA = 0b0001;
            MTU4.TIORH.BIT.IOB = 0b0010;
            MTU4.TGRA = counter_period;
            MTU4.TGRB = duty_cycle;
            MTU.TOER.BIT.OE4C = 1U;     // IO5 PWM
            MTU.TOER.BIT.OE4A = 1U;     // IO4 PWM
            MTU.TSTR.BIT.CST4 = 1U;
            success = true;
        }
        // TPU3, TIOCD3 ------------------------------------------------------/
        else if (pin == PIN_IO3)
        {
            // Configure the pin to use the peripheral module as an output.
            PORT2.PDR.BIT.B3 = 1U;
            PORT2.PMR.BIT.B3 = 1U;

            // Set the MTU function, and disable other pins on this channel.
            MPC.P23PFS.BIT.PSEL = 0b00011;

            // Stop the PWM signal.
            TPUA.TSTR.BIT.CST3 = 0U;
            // Set the counter to run at the desired frequency (12MHz*divisor).
            TPU3.TCR.BIT.TPSC = clock_divisor;
            // Set TGRB compare match to clear TCNT.
            TPU3.TCR.BIT.CCLR = 0b010;
            // Set the count to occur on rising edge of PCLK.
            TPU3.TCR.BIT.CKEG = 0b01;
            // Set PWM mode 2.
            TPU3.TMDR.BIT.MD = 0b0011;
            // Set TGRB to have an initial value of 0 and clear to 0.
            TPU3.TIORH.BIT.IOB = 0b0001;
            // Set TGRD to have an initial value of 0 and clear to 1.
            TPU3.TIORL.BIT.IOD = 0b0010;
            // Set the period.
            TPU3.TGRB = counter_period;
            // Set the duty cycle in TGRC.
            TPU3.TGRD = duty_cycle;
            // Start the PWM signal.
            TPUA.TSTR.BIT.CST3 = 1U;
            // Update the success flag.
            success = true;
        }
        // TPU3, TIOCC3 ------------------------------------------------------/
        else if (pin == PIN_IO2)
        {
            // Configure the pin to use the peripheral module as an output.
            PORT2.PDR.BIT.B2 = 1U;
            PORT2.PMR.BIT.B2 = 1U;

            // Set the MTU function, and disable other pins on this channel.
            MPC.P22PFS.BIT.PSEL = 0b00011;

            // Stop the PWM signal.
            TPUA.TSTR.BIT.CST3 = 0U;
            // Set the counter to run at the desired frequency (12MHz*divisor).
            TPU3.TCR.BIT.TPSC = clock_divisor;
            // Set TGRB compare match to clear TCNT.
            TPU3.TCR.BIT.CCLR = 0b010;
            // Set the count to occur on rising edge of PCLK.
            TPU3.TCR.BIT.CKEG = 0b01;
            // Set PWM mode 2.
            TPU3.TMDR.BIT.MD = 0b0011;
            // Set TGRB to have an initial value of 0 and clear to 0.
            TPU3.TIORH.BIT.IOB = 0b0001;
            // Set TGRC to have an initial value of 0 and clear to 1.
            TPU3.TIORL.BIT.IOC = 0b0010;
            // Set the period.
            TPU3.TGRB = counter_period;
            // Set the duty cycle in TGRC.
            TPU3.TGRC = duty_cycle;
            // Start the PWM signal.
            TPUA.TSTR.BIT.CST3 = 1U;
            // Update the success flag.
            success = true;
        }
        // TPU3, TIOCA3 ------------------------------------------------------/
        else if (pin == PIN_IO0)
        {
            // Configure the pin to use the peripheral module as an output.
            PORT2.PDR.BIT.B1 = 1U;
            PORT2.PMR.BIT.B1 = 1U;

            // Set the MTU function, and disable other pins on this channel.
            MPC.P21PFS.BIT.PSEL = 0b00011;

            // Stop the PWM signal.
            TPUA.TSTR.BIT.CST3 = 0U;
            // Set the counter to run at the desired frequency (12MHz*divisor).
            TPU3.TCR.BIT.TPSC = clock_divisor;
            // Set TGRB compare match to clear TCNT.
            TPU3.TCR.BIT.CCLR = 0b010;
            // Set the count to occur on rising edge of PCLK.
            TPU3.TCR.BIT.CKEG = 0b01;
            // Set PWM mode 2.
            TPU3.TMDR.BIT.MD = 0b0011;
            // Set TGRB to have an initial value of 0 and clear to 0.
            TPU3.TIORH.BIT.IOB = 0b0001;
            // Set TGRC to have an initial value of 0 and clear to 1.
            TPU3.TIORH.BIT.IOA = 0b0010;
            // Set the period.
            TPU3.TGRB = counter_period;
            // Set the duty cycle in TGRC.
            TPU3.TGRA = duty_cycle;
            // Start the PWM signal.
            TPUA.TSTR.BIT.CST3 = 1U;
            // Update the success flag.
            success = true;
        }
        // TPU2, for Software PWM -------------------------------------------------/
        else if (pin == PIN_IO9 || pin == PIN_IO10)
        {
            switch(pin){
            case PIN_IO9:
                pinMode(9, OUTPUT);
                digitalWrite(9, LOW);
                if (for_analog_write)
                {
                    g_swPwmPin9_counter_period = 255; // 490Hz
                    g_swPwmPin9_duty_cycle = g_swPwmPin9_counter_period - (g_swPwmPin9_counter_period * value / 255);
                } else {
                    g_swPwmPin9_counter_period = 125000 / frequency;
                    g_swPwmPin9_duty_cycle = g_swPwmPin9_counter_period / 2U;
                }
                break;
            case PIN_IO10:
                pinMode(10, OUTPUT);
                digitalWrite(10, LOW);
                if (for_analog_write)
                {
                    g_swPwmPin10_counter_period = 255; // 490Hz
                    g_swPwmPin10_duty_cycle = g_swPwmPin10_counter_period - (g_swPwmPin10_counter_period * value / 255);
                } else {
                    g_swPwmPin10_counter_period = 125000 / frequency;
                    g_swPwmPin10_duty_cycle = g_swPwmPin10_counter_period / 2U;
                }
                break;
            }

            // Stop the timer.
            TPUA.TSTR.BIT.CST2 = 0U;
            // Set the counter to run at the desired frequency.
            TPU2.TCR.BIT.TPSC = 0b011;
            // Set TGRA compare match to clear TCNT.
            TPU2.TCR.BIT.CCLR = 0b001;
            // Set the count to occur on rising edge of PCLK.
            TPU2.TCR.BIT.CKEG = 0b01;
            // Set Normal.
            TPU2.TMDR.BIT.MD = 0b0000;

            // Set the count to occur on rising edge of PCLK.
            TPU2.TSR.BIT.TGFA = 0U;
            /* Set TGI6A interrupt priority level to 5*/
            IPR(TPU2,TGI2A) = 0x5;
            /* Enable TGI6A interrupts */
            IEN(TPU2,TGI2A) = 0x1;
            /* Clear TGI6A interrupt flag */
            IR(TPU2,TGI2A) = 0x0;
            // Enable the module interrupt for the ms timer.
            TPU2.TIER.BIT.TGIEA = 1U;
            // Set the period.
            TPU2.TGRA = 5; // 125kHz
            // Start the timer.
            TPUA.TSTR.BIT.CST2 = 1U;

            success = true;
        }
        else
        {
            LOG_INFO(MODULE, "set_pwm_for_pin: Unmapped pin.");
        }

        // Deny access to the PFS registers and CGC registers.
        grant_pfs_write_access(false);

    } // if : pin check.
    else
    {
        LOG_INFO(MODULE, "set_pwm_for_pin: Invalid pin.");
    }

    if (success){
        if (for_analog_write){
            g_pinsCurrentAttachFunction[pin] = _ATTACH_ANALOGWRITE;
        } else {
            g_pinsCurrentAttachFunction[pin] = _ATTACH_TONE;
        }
    }


    return success;
}

bool rapid_set_pwm_for_pin(byte pin, byte clock_divisor, word counter_period, word duty_cycle){
    bool success = false;

    // Determine which pin we're trying to write to. This will determine
    // which multi-function timer unit we need to use.
    // TPU0, TIOCC0 ------------------------------------------------------/
    if (pin == PIN_IO6)
    {
        // Stop the PWM signal.
        TPUA.TSTR.BIT.CST0 = 0U;
        // Set the counter to run at the desired frequency (12MHz*divisor).
        TPU0.TCR.BIT.TPSC = clock_divisor;
        // Set the period.
        TPU0.TGRA = counter_period;
        // Set the duty cycle in TGRC.
        TPU0.TGRC = duty_cycle;
        // Start the PWM signal.
        TPUA.TSTR.BIT.CST0 = 1U;
        // Update the success flag.
        success = true;
    }
    // TPU0, TIOCD0 ------------------------------------------------------/
    else if (pin == PIN_IO7)
    {
        // Set the counter to run at the desired frequency (12MHz*divisor).
        TPU0.TCR.BIT.TPSC = clock_divisor;
        // Set the period.
        TPU0.TGRA = counter_period;
        // Set the duty cycle in TGRD.
        TPU0.TGRD = duty_cycle;
        // Start the PWM signal.
        TPUA.TSTR.BIT.CST0 = 1U;
        // Update the success flag.
        success = true;
    }
    // MTU1, MTIOC1A ------------------------------------------------------/
    else if (pin == PIN_IO1 || pin == PIN_IO48)
    {
        // Each MTU unit has different register settings to achieve the same
        // result - see the hardware manual for details.
        MTU.TSTR.BIT.CST1 = 0U;
        MTU1.TCR.BIT.TPSC = clock_divisor;
        MTU1.TGRA = counter_period;
        MTU1.TGRB = duty_cycle;
        MTU.TSTR.BIT.CST1 = 1U;
        success = true;
    }

    /* removed because MTIOC3C is used for PIN11, so that influence to clearTCNT
    // MTU3, MTIOC3A ------------------------------------------------------/
    else if (pin == PIN_IO32 || pin == PIN_IO35 || pin == PIN_IO23 || //
            pin == PIN_IO12)
    {
        switch (pin)
        {
        case PIN_IO32:
            PORT1.PDR.BIT.B4 = 1U;
            PORT1.PMR.BIT.B4 = 1U;
            MPC.P14PFS.BIT.ISEL = 0U;
            MPC.P14PFS.BIT.PSEL = 0b00001;
            MPC.P17PFS.BIT.PSEL = 0U;
            MPC.PC1PFS.BIT.PSEL = 0U;
            MPC.PC7PFS.BIT.PSEL = 0U;
            break;
        case PIN_IO35:
            PORT1.PDR.BIT.B7 = 1U;
            PORT1.PMR.BIT.B7 = 1U;
            MPC.P14PFS.BIT.PSEL = 0U;
            MPC.P17PFS.BIT.ISEL = 0U;
            MPC.P17PFS.BIT.PSEL = 0b00001;
            MPC.PC1PFS.BIT.PSEL = 0U;
            MPC.PC7PFS.BIT.PSEL = 0U;
            break;
        case PIN_IO23:
            PORTC.PDR.BIT.B1 = 1U;
            PORTC.PMR.BIT.B1 = 1U;
            MPC.P14PFS.BIT.PSEL = 0U;
            MPC.P17PFS.BIT.PSEL = 0U;
            MPC.PC1PFS.BIT.ISEL = 0U;
            MPC.PC1PFS.BIT.PSEL = 0b00001;
            MPC.PC7PFS.BIT.PSEL = 0U;
            break;
        case PIN_IO12:
            PORTC.PDR.BIT.B7 = 1U;
            PORTC.PMR.BIT.B7 = 1U;
            MPC.P14PFS.BIT.PSEL = 0U;
            MPC.P17PFS.BIT.PSEL = 0U;
            MPC.PC1PFS.BIT.PSEL = 0U;
            MPC.PC7PFS.BIT.ISEL = 0U;
            MPC.PC7PFS.BIT.PSEL = 0b00001;
            break;
        default:
            break;
        }

        MTU.TSTR.BIT.CST3= 0U;
        MTU3.TCR.BIT.TPSC = clock_divisor;
        MTU3.TCR.BIT.CCLR = 0b001;
        MTU3.TCR.BIT.CKEG = 0b01;
        MTU3.TMDR.BIT.MD = 0b0010;
        MTU3.TIORH.BIT.IOA = 0b0001;
        MTU3.TIORH.BIT.IOB = 0b0010;
        MTU3.TGRA = counter_period;
        MTU3.TGRB = duty_cycle;
        MTU.TSTR.BIT.CST3 = 1U;
        success = true;
    }
    */

    // MTU3, MTIOC3C ------------------------------------------------------/
    else if (pin == PIN_IO34 || pin == PIN_IO22 || pin == PIN_IO11 || //
            pin == PIN_IO55)
    {
        MTU.TSTR.BIT.CST3 = 0U;
        MTU3.TCR.BIT.TPSC = clock_divisor;
        MTU3.TGRC = counter_period;
        MTU3.TGRD = duty_cycle;
        MTU.TSTR.BIT.CST3 = 1U;
        success = true;
    }
    // TPU4, TIOCA4 ------------------------------------------------------/
    else if (pin == PIN_IO5)
    {
        // Stop the PWM signal.
        TPUA.TSTR.BIT.CST4 = 0U;
        // Set the counter to run at the desired frequency (12MHz*divisor).
        TPU4.TCR.BIT.TPSC = clock_divisor;
        // Set the period.
        TPU4.TGRA = counter_period;
        // Set the duty cycle in TGRB.
        TPU4.TGRB = duty_cycle;
        // Start the PWM signal.
        TPUA.TSTR.BIT.CST4 = 1U;
        // Update the success flag.
        success = true;
    }
    // MTU4, MTIOC4A ------------------------------------------------------/
    else if (pin == PIN_IO4 || pin == PIN_LED1 || pin == PIN_IO46)
    {
        MTU.TSTR.BIT.CST4 = 0U;
        MTU4.TCR.BIT.TPSC = clock_divisor;
        MTU4.TGRA = counter_period;
        MTU4.TGRB = duty_cycle;
        MTU.TSTR.BIT.CST4 = 1U;
        success = true;
    }
    // TPU3, TIOCD3 ------------------------------------------------------/
    else if (pin == PIN_IO3)
    {
        // Stop the PWM signal.
        TPUA.TSTR.BIT.CST3 = 0U;
        // Set the counter to run at the desired frequency (12MHz*divisor).
        TPU3.TCR.BIT.TPSC = clock_divisor;
        // Set the period.
        TPU3.TGRB = counter_period;
        // Set the duty cycle in TGRD.
        TPU3.TGRD = duty_cycle;
        // Start the PWM signal.
        TPUA.TSTR.BIT.CST3 = 1U;
        // Update the success flag.
        success = true;
    }
    // TPU3, TIOCC3 ------------------------------------------------------/
    else if (pin == PIN_IO2)
    {
        // Stop the PWM signal.
        TPUA.TSTR.BIT.CST3 = 0U;
        // Set the counter to run at the desired frequency (12MHz*divisor).
        TPU3.TCR.BIT.TPSC = clock_divisor;
        // Set the period.
        TPU3.TGRB = counter_period;
        // Set the duty cycle in TGRC.
        TPU3.TGRC = duty_cycle;
        // Start the PWM signal.
        TPUA.TSTR.BIT.CST3 = 1U;
        // Update the success flag.
        success = true;
    }
    // TPU3, TIOCA3 ------------------------------------------------------/
    else if (pin == PIN_IO0)
    {
        // Stop the PWM signal.
        TPUA.TSTR.BIT.CST3 = 0U;
        // Set the counter to run at the desired frequency (12MHz*divisor).
        TPU3.TCR.BIT.TPSC = clock_divisor;
        // Set the period.
        TPU3.TGRB = counter_period;
        // Set the duty cycle in TGRC.
        TPU3.TGRA = duty_cycle;
        // Start the PWM signal.
        TPUA.TSTR.BIT.CST3 = 1U;
        // Update the success flag.
        success = true;
    }
    else
    {
        LOG_INFO(MODULE, "set_pwm_for_pin: Unmapped pin.");
    }

    return success;


}

bool rapid_set_swPwm_for_pin(int pin, uint16_t frequency, byte value,
        bool for_analog_write){
    // TPU2, for Software PWM -------------------------------------------------/
    switch(pin){
    case PIN_IO9:
        if (for_analog_write)
        {
            g_swPwmPin9_counter_period = 255; // 490Hz
            g_swPwmPin9_duty_cycle = g_swPwmPin9_counter_period - (g_swPwmPin9_counter_period * value / 255);
        } else {
            g_swPwmPin9_counter_period = 125000 / frequency;
            g_swPwmPin9_duty_cycle = g_swPwmPin9_counter_period / 2U;
        }
        g_count_swpwmPin9 = 0;
        break;
    case PIN_IO10:
        if (for_analog_write)
        {
            g_swPwmPin10_counter_period = 255; // 490Hz
            g_swPwmPin10_duty_cycle = g_swPwmPin10_counter_period - (g_swPwmPin10_counter_period * value / 255);
        } else {
            g_swPwmPin10_counter_period = 125000 / frequency;
            g_swPwmPin10_duty_cycle = g_swPwmPin10_counter_period / 2U;
        }
        g_count_swpwmPin10 = 0;
        break;
    }

    return true;
}

/**
 * Generate a BRK instruction. This differs from the usual built-in variant
 * because we need to manually reset the user stack pointer. This is because
 * we do not transition to user mode when the program starts.
 */
void generate_brk()
{
    asm("brk");
    asm("mvtc #30000h, psw");
}

/****************************************************************************
 * Attach interval timer function
 *
 * The callback function is called every 1ms interval
 * @param[in] fFunction Specify callback function
 *
 * @return none
 *
 ***************************************************************************/
void attachIntervalTimerHandler(void (*fFunction)(unsigned long u32Milles))
{
    g_fITInterruptFunc = fFunction;

    startModule(MstpIdTPU0);

    // Stop the timer.
    TPUA.TSTR.BIT.CST5 = 0U;
    // Set the counter to run at the desired frequency.
    TPU5.TCR.BIT.TPSC = 0b011;
    // Set TGRA compare match to clear TCNT.
    TPU5.TCR.BIT.CCLR = 0b001;
    // Set the count to occur on rising edge of PCLK.
    TPU5.TCR.BIT.CKEG = 0b01;
    // Set Normal.
    TPU5.TMDR.BIT.MD = 0b0000;
    // Set the period.
    TPU5.TGRA = 750 - 1; //1ms setting at PCLK/64(750kHz)

    // Set the count to occur on rising edge of PCLK.
    TPU5.TSR.BIT.TGFA = 0U;

    /* Set TGI6A interrupt priority level to 5*/
    IPR(TPU5,TGI5A) = 0x5;
    /* Enable TGI6A interrupts */
    IEN(TPU5,TGI5A) = 0x1;
    /* Clear TGI6A interrupt flag */
    IR(TPU5,TGI5A) = 0x0;
    // Enable the module interrupt for the ms timer.
    TPU5.TIER.BIT.TGIEA = 1U;

    // Start the timer.
    TPUA.TSTR.BIT.CST5 = 1U;
}


/****************************************************************************
 * Detach interval timer function
 *
 * @param[in] none
 *
 * @return none
 *
 ***************************************************************************/
void detachIntervalTimerHandler()
{
    g_fITInterruptFunc = NULL;
    // Stop the timer.
    TPUA.TSTR.BIT.CST5 = 0U;

}

/****************************************************************************
 * Attach cyclic handler
 *
 * Attached handler is called every specified interval u32CyclicTime
 *
 * @param[in] u8HandlerNumber Specify ID from 0 to 7
 * @param[in] fFunction       Specify handler
 * @param[in] u32CyclicTime   Specify interval [ms]
 *
 * @return none
 *
 ***************************************************************************/
void attachCyclicHandler(uint8_t u8HandlerNumber, void (*fFunction)(unsigned long u32Milles), uint32_t u32CyclicTime)
{

    if (u8HandlerNumber < MAX_CYCLIC_HANDLER) {
        g_afCyclicHandler[u8HandlerNumber]              = fFunction;
        g_au32CyclicTime[u8HandlerNumber]               = u32CyclicTime;
        g_au32CyclicHandlerLastTime[u8HandlerNumber]    = millis();
    }

}

/****************************************************************************
 * Detach cyclic handler
 *
 * @param[in] ID from 0 to 7
 *
 * @return none
 *
 ***************************************************************************/
void detachCyclicHandler(uint8_t u8HandlerNumber)
{

    if (u8HandlerNumber < MAX_CYCLIC_HANDLER) {
        g_afCyclicHandler[u8HandlerNumber]              = NULL;
        g_au32CyclicTime[u8HandlerNumber]               = 0;
        g_au32CyclicHandlerLastTime[u8HandlerNumber]    = 0;
    }
}

/****************************************************************************
 * Execution cyclic handler
 *
 * @param[in] none
 *
 * @return none
 *
 ***************************************************************************/

void execCyclicHandler()
{
    int i;

    for (i = 0; i < MAX_CYCLIC_HANDLER; i++) {
        if (g_afCyclicHandler[i] != NULL) {
            unsigned long currentTime = millis();
            if ((currentTime - g_au32CyclicHandlerLastTime[i]) >= g_au32CyclicTime[i]) {
                g_au32CyclicHandlerLastTime[i] = currentTime;
                (*g_afCyclicHandler[i])(currentTime);
            }
        }
    }
}

/****************************************************************************
 * Attach interval timer function
 *
 * The callback function is called every 1ms interval
 * @param[in] fFunction Specify callback function
 *
 * @return always 1
 *
 ***************************************************************************/
unsigned long timer_regist_userfunc(void (*fFunction)(void))
{
    if (fFunction != NULL) {
        attachIntervalTimerHandler((void (*)(unsigned long))fFunction);
    } else {
        detachIntervalTimerHandler();
    }
    return 1;
}

// INTERRUPT HANDLERS *********************************************************/
// Note that these are declared in interrupts_handlers.h but defined here for
// clarity.

// TPU5 TGI5A
void INT_Excep_TPU5_TGI5A(void){
    if (g_fITInterruptFunc != NULL) {
        (*g_fITInterruptFunc)(millis());
    }
}

// TPU2 TGI2A
void INT_Excep_TPU2_TGI2A(void){
    if (g_count_swpwmPin9 == 0) {
        if (g_swPwmPin9_duty_cycle > 0) {
            digitalWrite(9, LOW);
        } else {
            digitalWrite(9, HIGH);
        }
    }
    if (g_count_swpwmPin9 == g_swPwmPin9_duty_cycle) {
        digitalWrite(9, HIGH);
    }
    g_count_swpwmPin9++;
    if (g_count_swpwmPin9 >= g_swPwmPin9_counter_period){ // over flow
        g_count_swpwmPin9 = 0;
    }

    if (g_count_swpwmPin10 == 0) {
        if (g_swPwmPin10_duty_cycle > 0) {
            digitalWrite(10, LOW);
        } else {
            digitalWrite(10, HIGH);
        }
    }
    if (g_count_swpwmPin10 == g_swPwmPin10_duty_cycle) {
        digitalWrite(10, HIGH);
    }
    g_count_swpwmPin10++;
    if (g_count_swpwmPin10 >= g_swPwmPin10_counter_period){ // over flow
        g_count_swpwmPin10 = 0;
    }
}
