/***************************************************************************
PURPOSE
     RX63N Library for Arduino compatible framework

TARGET DEVICE
     RX63N

AUTHOR
     Renesas Solutions Corp.
     AND Technology Research Ltd.

***************************************************************************
Copyright (C) 2014 Renesas Electronics. All rights reserved.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

See file LICENSE.txt for further informations on licensing terms.

***************************************************************************
 Copyright  : (c) AND Technology Research Ltd, 2013
 Address    : 4 Forest Drive, Theydon Bois, Essex, CM16 7EY
 Tel        : +44 (0) 1992 81 4655
 Fax        : +44 (0) 1992 81 3362
 Email      : ed.king@andtr.com
 Website    : www.andtr.com

 Project    : Arduino
 Module     : Utilities
 File       : log.cpp
 Author     : E King
 Start Date : 19/02/13
 Description: Implementation of the Log class.

******************************************************************************/
/*
 *  Modified 9 May 2014 by Yuuki Okamiya, for remove warnings
 */

#include <stdio.h>
#include "log.h"

// DEFINES ********************************************************************/

// DECLARATIONS ***************************************************************/

/** A log buffer. */
#ifdef DEBUG
//static char s_log_buffer[LOG_LINE_LENGTH];
#endif

// DEFINITIONS ****************************************************************/

/**
 * Log a DEBUG level message.
 */
void Log::log_debug(const char *module, const char *message)
{
#ifdef DEBUG
//    snprintf(s_log_buffer, sizeof(s_log_buffer), "DEBUG  : %s: %s",
//            module, message);
//    printf("%s\n", s_log_buffer);
#endif
}

/**
 * Log an INFO level message.
 */
void Log::log_info(const char *module, const char *message)
{
#ifdef DEBUG
//    snprintf(s_log_buffer, sizeof(s_log_buffer), "INFO   : %s: %s",
//        module, message);
//    printf("%s\n", s_log_buffer);
#endif
}

/**
 * Log a WARNING level message.
 */
void Log::log_warning(const char *module, const char *message)
{
#ifdef DEBUG
//    snprintf(s_log_buffer, sizeof(s_log_buffer), "WARNING: %s: %s",
//        module, message);
//    printf("%s\n", s_log_buffer);
#endif
}

/**
 * Log an ERROR level message.
 */
void Log::log_error(const char *module, const char *message)
{
#ifdef DEBUG
//    snprintf(s_log_buffer, sizeof(s_log_buffer), "ERROR  : %s: %s",
//            module, message);
//    printf("%s\n", s_log_buffer);
#endif
}

/**
 * Log a FATAL level message.
 */
void Log::log_fatal(const char *module, const char *message)
{
#ifdef DEBUG
//    snprintf(s_log_buffer, sizeof(s_log_buffer), "FATAL  : %s: %s",
//            module, message);
//    printf("%s\n", s_log_buffer);
#endif
}
