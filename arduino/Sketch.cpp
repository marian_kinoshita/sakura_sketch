/* GR-SAKURA Sketch Template E2.00j */
#include <Arduino.h>
#include "Usb.h"
#include "cdcftdi.h"



class FTDIAsync : public FTDIAsyncOper
{
public:
    virtual uint8_t OnInit(FTDI *pftdi);
    virtual uint8_t OnRelease(FTDI *pftdi);
};

uint8_t FTDIAsync::OnRelease(FTDI *pftdi)
{
    uint8_t rcode = 0;
    /* Things to do when releasing*/

    return rcode;
}

uint8_t FTDIAsync::OnInit(FTDI *pftdi)
{
    uint8_t rcode = 0;

    rcode = pftdi->SetBaudRate(115200);

    if (rcode)
    {
        ErrorMessage<uint8_t>(PSTR("SetBaudRate"), rcode);
        return rcode;
    }
    rcode = pftdi->SetFlowControl(FTDI_SIO_DISABLE_FLOW_CTRL);

    if (rcode)
        ErrorMessage<uint8_t>(PSTR("SetFlowControl"), rcode);
#if defined(_DEBUG_)
    /* Turn ON to indicate that we have configured FTDI. */
    PORTA.PODR.BIT.B0 = 1;
#endif

    return rcode;
}
/* Added dummy classes for testing. */
USB              Usb;
FTDIAsync        FtdiAsync;
FTDI             Ftdi(&Usb, &FtdiAsync);

void setup(){

    pinMode(PIN_LED1, OUTPUT);
    pinMode(PIN_LED0, OUTPUT);
    Serial.begin(9600); // for USB CDC

}

void loop(){

    if(digitalRead(PIN_SW) == 1){
        Serial.println("Hello Im SAKURA");
        digitalWrite(PIN_LED0, HIGH);
        delay(200);
        digitalWrite(PIN_LED0, LOW);
        delay(200);
    } else {
        Serial.println("It's fun to make with everyone!");
        digitalWrite(PIN_LED1, HIGH);
        delay(200);
        digitalWrite(PIN_LED1, LOW);
        delay(200);
    }

}
